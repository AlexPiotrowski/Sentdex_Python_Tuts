
print('This is an example of print function')

print('we\'re going to the store')

print('He said "Hi."')

print('Hi ' + 'there')

print('Hi', 'there')

print('Hi', 5)

#print('Hi'+5)         gives an error
#print(5+'Hi')         gives an error

print('Hi '+str(5))

print(float('8.5')+5)
