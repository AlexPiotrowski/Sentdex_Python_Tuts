
x = 6

def example():
    global x            #converts x to a global variable
    print(x)
    x += 5              #would produce an error if x is not global
    print(x)
#most people do like global variables


#an alternative if you do not want to use global variables
def example2():
    globx = x
    print(globx)
    globx+=5
    print(globx)

    return globx
    

example()

x = example2()

print(x)
