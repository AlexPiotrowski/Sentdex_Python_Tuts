
x = 5
y = 8
z = 5
a = 3

if x > 7:
    print('x is greater than y')

if z < y > x:
    print('y is greater than z and greater than x')

if z < y > x > a:
    print('y is greater than z and greater than x and a')

if z <= x:
    print('z is less than or equal to x')

if z == x:
    print('z is equal to x')

if z != y:
    print('z is not equal to y')
