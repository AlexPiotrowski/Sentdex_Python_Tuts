from matplotlib import pyplot as plt
from matplotlib import style
import numpy as np

style.use('ggplot')

x,y = np.loadtxt('exampleFile.csv',
                 unpack = True,
                 delimiter = ',')    #both csv and txt works

#print(x)
#print(y)           #They are arrays

plt.plot(x,y)

plt.title('Epic Chart')
plt.ylabel('Y axis')
plt.xlabel('X axis')

plt.show()
