
def simple_addition(num1, num2):
    answer = num1 + num2
    print('num1 is',num1)
    print(answer)

simple_addition(5,3)

simple_addition(3,5)

simple_addition(num1=5,num2=3)

simple_addition(num2=5,num1=3)

#simple_addition(3)          gives error only one argument given
#simple_addition(3,5,4)      gives error too, too many arguments (3)
