#list is mutable (can be changed)
#tuple is immutable (can't be changed)

#tuples
x = 5,6,2,6
#x = (5,6,2,6)

#lists
y = [5,6,2,6]

#def exampleFunc():
 #   return 15,6
##
#x,y = exampleFunc()
#print(x)

print(x[1])
print(y[2], y[3])
