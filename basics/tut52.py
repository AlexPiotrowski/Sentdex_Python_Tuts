from matplotlib import pyplot as plt
from matplotlib import style

#style.use('ggplot')
#style.use('dark_background')
#style.use('grayscale')

x = [2,4,6,8] 
y = [7,3,8,3]

x2 = [1,3,5,7] 
y2 = [6,5,3,9]

#plt.scatter(x,y)

#plt.scatter(x2,y2)

plt.bar(x,y, align = 'center')

plt.bar(x2,y2, align = 'center')

plt.title('Epic Chart')
plt.ylabel('Y axis')
plt.xlabel('X axis')

plt.show()
