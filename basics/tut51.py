from matplotlib import pyplot as plt
from matplotlib import style

#style.use('ggplot')
#style.use('dark_background')
#style.use('grayscale')

x = [5,6,7,8] 
y = [7,3,8,3]

x2 = [5,6,7,8] 
y2 = [6,5,3,8]

plt.grid(True, color='k')
plt.plot(x,y,'g',linewidth=5, label='line 1')

plt.plot(x2,y2,'c',linewidth=10, label='line 2')

plt.title('Epic Chart')
plt.ylabel('Y axis')
plt.xlabel('X axis')
plt.legend()



plt.show()
